﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionController : MonoBehaviour
{

    public GameObject playerGameObject;
    public GameObject axeGameObject;
    public float Speed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "SwordCube")
        {
            iTween.MoveTo(playerGameObject, axeGameObject.transform.position, Speed);
            playerGameObject.GetComponent<Animator>().SetBool("walking", true);
        }
    }

}