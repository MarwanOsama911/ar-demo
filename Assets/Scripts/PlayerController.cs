﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject axeGameObject;
    public GameObject axeGameObjectHolder;
    public float speed;

    Vector3 playerIntialPosition;

    private void Start()
    {
        playerIntialPosition = transform.position;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Sword")
        {

            StartCoroutine(PickUpAxe());
        }
    }
    IEnumerator PickUpAxe()
    {
        //pick up animation
        gameObject.GetComponent<Animator>().SetBool("walking", false);
        gameObject.GetComponent<Animator>().SetBool("pickingup", true);
        yield return new WaitForSeconds(1.3f);
        axeGameObject.transform.parent = axeGameObjectHolder.transform;
        axeGameObject.transform.localPosition = new Vector3(-0.0990008f, 0.07899698f, 0.0519962f);
        axeGameObject.transform.localRotation = Quaternion.EulerRotation(new Vector3(100f, 0f, 90f));
        //walk back
        yield return new WaitForSeconds(4f);
        gameObject.transform.localRotation = Quaternion.EulerRotation(new Vector3(180f, 90f, -90f));
        iTween.MoveTo(gameObject, playerIntialPosition, speed);
    }
}

